Replication Code Test  

Requirements  
Everything should be in C++, with variables (if and where necessary) exposed in Blueprints. The test should be created in Unreal Engine version 5

Part 1 - Rube Goldberg  
GOAL  
To build simple conversion machines which ingest specific assets and convert them into a new asset type.  IE: Ingest 2 triangles and output 1 square

- Use the First Person template or start from scratch
- Create a simple library of shape items using data tables (sphere, cube, rectangular box, cylinder, etc)
- Create a generic static actor that will act as a machine. The actor can be represented by a simple mesh in game
- Create a data table of “recipes” that combine a single shape or multiple shapes into a different shape.  Each Machine would have a different set of -
- Spawn shape actors around the map that the player can either pickup/drop or push around on the ground
- If a machine has enough shapes nearby, it will look up a valid recipe, destroy the ingredient items and spit out a new shape from the recipe

Part 2 - Gameplay Replication

Setup
- Place at least 4 machines in a level
- Create at least 10 unique recipes in the recipes data table
- This test requires a dedicated server running with at least one client
- All replication must use push model replication

Button Input
- Create a button actor that responds to user input
- The button must accept two different input keys (such as left mouse button and right mouse button)
- The client must be able to push the button and have it send a message to the server
- Every machine must have a button attached to it

Gameplay
- Pressing the button with input1 will complete a random recipe without needing the ingredients
- The spawning logic is handled on the server and the result is replicated to the client
- Pressing the button with input2 will toggle the machine on and off
- A machine that is off will not process any recipes
- The client should see a simple effect that lets them know whether the machine is on or off. This can be a simple color change, a looping sound, a visual effect, etc.
- When the machine successfully completes a recipe, an effect should be played on the client near where the recipe item is output
- Failing to spawn a recipe due to the machine being off must give the player some feedback
