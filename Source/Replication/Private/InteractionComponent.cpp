// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionComponent.h"

#include "InteractableInterface.h"
#include "GameFramework/Character.h"

static TAutoConsoleVariable<bool> CVarDrawDebugInteraction(TEXT("ac.DrawDebugInteraction"), true, TEXT("Enable debug draws on InteractionComponent."), ECVF_Cheat);

// Sets default values for this component's properties
UInteractionComponent::UInteractionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UInteractionComponent::Interact()
{
	UE_LOG(LogTemp, Log, TEXT("PrimaryInteract"));
	AActor* HitActor = nullptr;
	bool bFoundInteractable = FindInteractableActorOnSweep(&HitActor);
	if (bFoundInteractable)
	{
		ServerInteract(HitActor);
	}
}

void UInteractionComponent::SecondaryInteract()
{
	UE_LOG(LogTemp, Log, TEXT("PrimaryInteract"));
	AActor* HitActor = nullptr;
	bool bFoundInteractable = FindInteractableActorOnSweep(&HitActor);
	if (bFoundInteractable)
	{
		ServerSecondaryInteract(HitActor);
	}
}

bool UInteractionComponent::FindInteractableActorOnSweep(AActor** HitActor)
{
	bool bDebugDraw = CVarDrawDebugInteraction.GetValueOnGameThread();

	FCollisionObjectQueryParams ObjectQueryParams;
	ObjectQueryParams.AddObjectTypesToQuery(ECC_WorldDynamic);
	ObjectQueryParams.AddObjectTypesToQuery(ECC_PhysicsBody);

	FVector EyeLocation;
	FRotator EyeRotation;
	AActor* MyOwner = GetOwner();
	ACharacter* MyCharacter = Cast<ACharacter>(MyOwner);
	AController* MyController = MyCharacter->GetController();
	MyController->GetPlayerViewPoint(EyeLocation, EyeRotation);
	// MyCharacter->GetActorEyesViewPoint(EyeLocation, EyeRotation);
	FVector End = EyeLocation + (EyeRotation.Vector() * 1000);
	TArray<FHitResult> Hits;
	FCollisionShape Shape;
	Shape.SetSphere(30.0f);
	bool bBlockingHit = GetWorld()->SweepMultiByObjectType(Hits, EyeLocation, End, FQuat::Identity, ObjectQueryParams, Shape);
	FColor LineColor = bBlockingHit ? FColor::Green : FColor::Orange;

	bool bFoundInteractable = false;

	for (FHitResult Hit : Hits)
	{
		AActor* HitActorCheck = Hit.GetActor();
		if (HitActorCheck)
		{
			if (HitActorCheck->Implements<UInteractableInterface>())
			{
				if (bDebugDraw) DrawDebugSphere(GetWorld(), Hit.ImpactPoint, Shape.GetSphereRadius(), 2, LineColor, false, 2.0f);
				*HitActor = &*HitActorCheck;
				bFoundInteractable = true;
				break;
			}
		}
	}

	if (bDebugDraw)
	{
		DrawDebugLine(GetWorld(), EyeLocation, End, LineColor, false, 2.0f, 0, 2.0f);
	}

	return bFoundInteractable;
}

void UInteractionComponent::ServerInteract_Implementation(AActor* HitActor)
{
	IInteractableInterface::Execute_Interact(HitActor, Cast<APawn>(GetOwner()));
}

void UInteractionComponent::ServerSecondaryInteract_Implementation(AActor* HitActor)
{
	IInteractableInterface::Execute_SecondaryInteract(HitActor, Cast<APawn>(GetOwner()));
}
