// Fill out your copyright notice in the Description page of Project Settings.


#include "Machine.h"

#include "FRecipes.h"
#include "Shape.h"
#include "StaticMeshDescription.h"
#include "Components/BoxComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AMachine::AMachine()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	MeshComp->SetupAttachment(RootComponent);
	BoxComponent = CreateDefaultSubobject<UBoxComponent>("BoxComponent (Ingredients holder)");
	BoxComponent->SetupAttachment(RootComponent);
	SpawnPointForResultComponent = CreateDefaultSubobject<USceneComponent>("SpawnPoint");
	SpawnPointForResultComponent->SetupAttachment(RootComponent);
	LightIndicatorMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("LightIndicatorMeshComponent");
	LightIndicatorMeshComponent->SetupAttachment(RootComponent);
	MachineButton = CreateDefaultSubobject<UChildActorComponent>("MachineButton");
	MachineButton->SetupAttachment(RootComponent);

	SetReplicates(true);
}

void AMachine::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams SharedParams;
	SharedParams.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(AMachine, bIsPowerOn, SharedParams);
	DOREPLIFETIME_WITH_PARAMS_FAST(AMachine, StoredIngredients, SharedParams);
}

void AMachine::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	UMaterialInterface* Material = LightIndicatorMeshComponent->GetMaterial(0);
	MaterialDynamic = UMaterialInstanceDynamic::Create(Material, NULL);
	LightIndicatorMeshComponent->SetMaterial(0, MaterialDynamic);

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AMachine::OnBoxBeginOverlap);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &AMachine::OnBoxEndOverlap);
}

void AMachine::OnBoxBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bArg, const FHitResult& HitResult)
{
	if (!HasAuthority())
	{
		return;
	}

	AShape* Shape = Cast<AShape>(OtherActor);
	if (Shape)
	{
		StoredIngredients.Add(Shape);
		MARK_PROPERTY_DIRTY_FROM_NAME(AMachine, StoredIngredients, this);
		// StoredIngredients.MarkItemDirty(StoredIngredients.Items.Add_GetRef(Shape));
		CheckForIngredients();
	}
}

void AMachine::OnBoxEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int OtherBodyIndex)
{
	if (!HasAuthority())
	{
		return;
	}

	AShape* Shape = Cast<AShape>(OtherActor);
	if (Shape)
	{
		if (StoredIngredients.Contains(Shape))
		{
			StoredIngredients.Remove(Shape);
			MARK_PROPERTY_DIRTY_FROM_NAME(AMachine, StoredIngredients, this);
		}
	}
}

void AMachine::CheckForIngredients()
{
	for (FDataTableRowHandle RowHandle : Recipes)
	{
		FRecipes* Recipe = RowHandle.GetRow<FRecipes>(TEXT("Getting machine recipe"));
		if (Recipe)
		{
			bool HasAllIngredients = true;
			TArray<AShape*> IngredientsToConsume;

			for (UShapeData* RecipeIngredientShapeData : Recipe->Ingredients)
			{
				bool bFoundShape = false;
				for (AShape* StoredShape : StoredIngredients)
				{
					if (StoredShape->GetShapeData() == RecipeIngredientShapeData)
					{
						if (!IngredientsToConsume.Contains(StoredShape))
						{
							IngredientsToConsume.Add(StoredShape);
							bFoundShape = true;
							break;
						}
					}
				}
				if (!bFoundShape)
				{
					HasAllIngredients = false;
					IngredientsToConsume.Empty();
					break;
				}
			}

			if (!HasAllIngredients)
			{
				break;
			}

			if (HasAllIngredients)
			{
				if (bIsPowerOn)
				{
					ServerConsumeIngredients(IngredientsToConsume);
					ServerCookRecipe(Recipe->Result);
				}
			}
		}
	}
}

void AMachine::OnRep_bIsPowerOn() const
{
	FLinearColor Color = bIsPowerOn ? FLinearColor::Green : FLinearColor::Black;
	MaterialDynamic->SetVectorParameterValue(BaseColorMaterialName, Color);
	LightIndicatorMeshComponent->SetMaterial(0, MaterialDynamic);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), TogglePowerSound, GetActorLocation(), GetActorRotation());
}

void AMachine::SpawnFailed_Implementation()
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), SpawnFailSound, SpawnPointForResultComponent->GetComponentLocation(), SpawnPointForResultComponent->GetComponentRotation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SpawnFailParticle, SpawnPointForResultComponent->GetComponentLocation(), SpawnPointForResultComponent->GetComponentRotation());
}

void AMachine::SpawnSuccess_Implementation()
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), SpawnSuccessSound, SpawnPointForResultComponent->GetComponentLocation(), SpawnPointForResultComponent->GetComponentRotation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SpawnSuccessParticle, SpawnPointForResultComponent->GetComponentLocation(), SpawnPointForResultComponent->GetComponentRotation());
}

void AMachine::ServerTogglePower_Implementation()
{
	bIsPowerOn = !bIsPowerOn;
	MARK_PROPERTY_DIRTY_FROM_NAME(AMachine, bIsPowerOn, this);
	OnRep_bIsPowerOn();
	CheckForIngredients();
}

void AMachine::ServerCookRandomRecipe_Implementation()
{
	if (bIsPowerOn)
	{
		int32 RandomRecipeIndex = FMath::RandRange(0, Recipes.Num() - 1);
		FDataTableRowHandle RecipeRowHandle = Recipes[RandomRecipeIndex];
		FRecipes* Recipe = RecipeRowHandle.GetRow<FRecipes>(TEXT("Getting machine recipe"));

		for (FDataTableRowHandle RowHandle : Recipes)
		{
			Recipe = RowHandle.GetRow<FRecipes>("Getting machine recipe");
			break;
		}
		ServerCookRecipe(Recipe->Result);
	}
	else
	{
		SpawnFailed();
	}
}

void AMachine::ServerCookRecipe_Implementation(const TArray<UShapeData*>& Results)
{
	for (UShapeData* ShapeData : Results)
	{
		auto SpawnedActor = GetWorld()->SpawnActor<AActor>(ShapeData->ShapeClass, SpawnPointForResultComponent->GetComponentLocation(), SpawnPointForResultComponent->GetComponentRotation());
		if (SpawnedActor)
		{
			auto Component = Cast<UStaticMeshComponent>(SpawnedActor->GetComponentByClass(UStaticMeshComponent::StaticClass()));
			if (Component)
			{
				Component->AddImpulse(SpawnPointForResultComponent->GetForwardVector() * SpawnImpulse);
			}
			SpawnSuccess();
		}
	}
}

void AMachine::ServerConsumeIngredients_Implementation(const TArray<AShape*>& IngredientsToConsume)
{
	for (AShape* IngredientToConsume : IngredientsToConsume)
	{
		IngredientToConsume->Destroy();
	}
}
