// Fill out your copyright notice in the Description page of Project Settings.


#include "Shape.h"

#include "Replication/ReplicationCharacter.h"

// Sets default values
AShape::AShape()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("BarrelMesh");
	RootComponent = MeshComp;
	MeshComp->SetSimulatePhysics(true);
	MeshComp->SetCollisionProfileName("PhysicsActor");
	SetReplicates(true);
	AActor::SetReplicateMovement(true);
}

void AShape::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AShape::Interact_Implementation(APawn* InstigatorPawn)
{
	IInteractableInterface::Interact_Implementation(InstigatorPawn);

	auto Character = Cast<AReplicationCharacter>(InstigatorPawn);
	if (Character)
	{
		Character->ServerAttachShape(this);
	}
}

