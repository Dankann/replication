// Fill out your copyright notice in the Description page of Project Settings.


#include "MachineButton.h"

#include "Machine.h"

// Sets default values
AMachineButton::AMachineButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>("BaseMesh");
	RootComponent = BaseMesh;
}

void AMachineButton::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	Machine = Cast<AMachine>(GetParentActor());
}

void AMachineButton::Interact_Implementation(APawn* InstigatorPawn)
{
	Machine->ServerCookRandomRecipe();
}

void AMachineButton::SecondaryInteract_Implementation(APawn* InstigatorPawn)
{
	Machine->ServerTogglePower();
}

