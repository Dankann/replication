// Fill out your copyright notice in the Description page of Project Settings.


#include "ReplicationPlayerController.h"

#include "Blueprint/UserWidget.h"

void AReplicationPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if(!IsLocalController())
	{
		return;
	}
	
	if(IsValid(CrosshairWidget))
	{
		UUserWidget* Crosshair = CreateWidget(this, CrosshairWidget);
		if(Crosshair)
		{
			Crosshair->AddToViewport();
		}
	}	
}
