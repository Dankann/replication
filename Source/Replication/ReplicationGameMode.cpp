// Copyright Epic Games, Inc. All Rights Reserved.

#include "ReplicationGameMode.h"

#include "EngineUtils.h"
#include "Machine.h"
#include "ReplicationCharacter.h"
#include "UObject/ConstructorHelpers.h"

AReplicationGameMode::AReplicationGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}