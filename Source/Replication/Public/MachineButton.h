// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractableInterface.h"
#include "GameFramework/Actor.h"
#include "MachineButton.generated.h"

class AMachine;
UCLASS()
class REPLICATION_API AMachineButton : public AActor, public IInteractableInterface
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BaseMesh;
private:
	UPROPERTY()
	AMachine* Machine;
public:
	// Sets default values for this actor's properties
	AMachineButton();

	virtual void Interact_Implementation(APawn* InstigatorPawn) override;

	virtual void SecondaryInteract_Implementation(APawn* InstigatorPawn) override;

private:
	virtual void PostInitializeComponents() override;
};
