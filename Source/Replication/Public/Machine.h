// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FRecipes.h"
#include "Engine/DataTable.h"
#include "GameFramework/Actor.h"
#include "Machine.generated.h"

class USoundCue;
class AMachineButton;
class AShape;
class UDataTable;
class UBoxComponent;

UCLASS()
class REPLICATION_API AMachine : public AActor
{
	GENERATED_BODY()

public:
	bool GetIsPowerOn() const
	{
		return bIsPowerOn;
	}

protected:
	UPROPERTY(VisibleAnywhere)
	FName BaseColorMaterialName = "Base Color";

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MeshComp;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* LightIndicatorMeshComponent;

	UPROPERTY(EditAnywhere)
	USceneComponent* SpawnPointForResultComponent;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere)
	TArray<FDataTableRowHandle> Recipes;

	// UPROPERTY(Replicated, VisibleAnywhere)
	// FShapeFastArray StoredIngredients;

	UPROPERTY(Replicated, VisibleAnywhere)
	TArray<AShape*> StoredIngredients;

	UPROPERTY(EditAnywhere)
	float SpawnImpulse = 5000.0f;

	UPROPERTY()
	UMaterialInstanceDynamic* MaterialDynamic;

	UPROPERTY(ReplicatedUsing = "OnRep_bIsPowerOn")
	bool bIsPowerOn;

	UPROPERTY(EditAnywhere)
	UChildActorComponent* MachineButton;

	UPROPERTY(EditAnywhere)
	USoundBase* TogglePowerSound;

	UPROPERTY(EditAnywhere)
	USoundBase* SpawnSuccessSound;

	UPROPERTY(EditAnywhere)
	USoundBase* SpawnFailSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* SpawnSuccessParticle;

	UPROPERTY(EditAnywhere)
	UParticleSystem* SpawnFailParticle;

public:
	// Sets default values for this actor's properties
	AMachine();

	virtual void PostInitializeComponents() override;

	void CheckForIngredients();

	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bArg, const FHitResult& HitResult);
	UFUNCTION()
	void OnBoxEndOverlap(UPrimitiveComponent* PrimitiveComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int OtherBodyIndex);

	UFUNCTION(Server, Reliable)
	void ServerCookRecipe(const TArray<UShapeData*>& Results);

	UFUNCTION(Server, Reliable)
	void ServerConsumeIngredients(const TArray<AShape*>& IngredientToConsume);

	UFUNCTION(Server, Reliable)
	void ServerTogglePower();

	UFUNCTION(Server, Reliable)
	void ServerCookRandomRecipe();

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnFailed();

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnSuccess();

private:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION()
	void OnRep_bIsPowerOn() const;
	
};
