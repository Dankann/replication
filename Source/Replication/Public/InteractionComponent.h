// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InteractionComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class REPLICATION_API UInteractionComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInteractionComponent();

	void Interact();
	void SecondaryInteract();
	bool FindInteractableActorOnSweep(AActor** HitActor);

protected:
	UFUNCTION(Server, Reliable)
	void ServerInteract(AActor* HitActor);

	UFUNCTION(Server, Reliable)
	void ServerSecondaryInteract(AActor* HitActor);
};
