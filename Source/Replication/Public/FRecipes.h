// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "FRecipes.generated.h"

class UShapeData;

USTRUCT(BlueprintType)
struct FRecipes : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UShapeData*> Ingredients;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UShapeData*> Result;
};
