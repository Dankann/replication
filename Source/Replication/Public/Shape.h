// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractableInterface.h"
#include "ShapeData.h"
#include "GameFramework/Actor.h"
#include "Shape.generated.h"

UCLASS()
class REPLICATION_API AShape : public AActor, public IInteractableInterface
{
	GENERATED_BODY()

public:
	UShapeData* GetShapeData() const
	{
		return ShapeData;
	}

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MeshComp;
	
protected:
	UPROPERTY(EditDefaultsOnly)
	UShapeData* ShapeData;

public:
	// Sets default values for this actor's properties
	AShape();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Interact_Implementation(APawn* InstigatorPawn) override;

};
