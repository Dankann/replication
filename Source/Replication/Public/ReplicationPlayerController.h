// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ReplicationPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AReplicationPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> CrosshairWidget;

protected:
	virtual void BeginPlay() override;
};
